jQuery(document).ready(function ($) {
    $(".his_months-item_title").click(function () {
        $(this).toggleClass('active_month');
        $(this).next('.his_months-list').slideToggle(300);
        return false;
    });

    jQuery(document).ready(function ($) {
        $('#go').click(function (event) {
            event.preventDefault();
            $('#overlay').fadeIn(400,
                function () {
                    $('#modal_form')
                        .css('display', 'flex')
                        .animate({opacity: 1}, 200);
                });
        });

        $('#modal_close, #overlay').click(function () {
            $('#modal_form')
                .animate({opacity: 0}, 200,
                    function () {
                        $(this).css('display', 'none');
                        $('#overlay').fadeOut(400);
                    }
                );
        });
    });
});